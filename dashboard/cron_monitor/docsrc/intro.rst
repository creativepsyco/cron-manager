*****************
Introduction
*****************

Motivation
=================
The UNIX utility cron is good at scheduling tasks to run. However, it is not
as good at other things, most notably in its handling of output. The goal of this
is to provide a better interface for how cron jobs are run, and what information
is received from the commands run.

Cron Monitor uses Django to make a web dashboard that displays information about cron runs in
a cleaner and more visible way.

Features
============
* Tracks the start time, run time, and exit codes of commands
* Color codes output based on whether it is from standard out or standard error
* Can send email based on a job's exit code and standard error output
